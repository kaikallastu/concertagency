<?php
include("model.php");
include("securimage/securimage.php");
error_reporting(E_ALL);
ini_set('display_errors', 1);

if (isset($_POST['captcha_code'])) {
    $securimage = new Securimage();
    $check = $securimage->check($_POST['captcha_code']);
    if ($check == false) {
        // the code was incorrect
        // you should handle the error so that the form processor doesn't continue

        // or you can use the following code if there is no validation or you do not know how
        echo "The security code entered was incorrect.<br /><br />";
        echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
        ///header("Location: order.php");
    }
    echo "CAPTCHA TÖÖTAB!?";
}

if (!empty($_POST['rsubmit'])) {
    $concertId = $_POST['concertId'];
    $startDate = $_POST['startDate'];
    $startTime = $_POST['startTime'];
    $duration = $_POST['duration'];
    $personCount = $_POST['personCount'];
    $contactType = $_POST['contactType'];
    $contactName = $_POST['contactName'];
    $contact = $_POST['contact'];
    $comment = $_POST['comment'];

    model_addReservation($concertId, $startDate, $startTime, $duration, $personCount, $contactType, $contactName, $comment, $contact);
    echo '<script>';
    echo 'alert("Your reservation successfully entered!");';
    echo '</script>';
    header("Location: reservation.php");
}