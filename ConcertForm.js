function resetForm(form) {
    form.reset();
}

function emailSentMsg(inp) {
    var parent = inp.parentNode;
    var formInputs = parent.getElementsByTagName("input");

    formInputs = [].slice.call(formInputs); // HTML Collection to an array -- hackish
    formInputs.forEach(function (inp) {
        if (inp.getAttribute("type") == "email" && inp.type == "email") {
            alert("email sent to: " + inp.value);
        }
    });

    resetForm(parent);
}
