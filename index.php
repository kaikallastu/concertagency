﻿<?php
include("header.php");
?>

<div class="content">
    <div class="welcome">
        <p>Welcome to our ConcertAgency site!</p>
    </div>
    <table>
        <?php foreach (model_getConcerts(3) as $row): ?>
            <tr>
                <td>
                    <div class="title"><?= $row["name"] ?></div>
                    <?php
                    $date = DateTime::createFromFormat("Y-m-d H:i:s", $row["date"] . " " . $row["time"]);
                    echo $date->format("l, jS \of F Y \a\\t G.i");
                    ?>
                    <br>
                    <?= $row["address"] ?><br><br>
                    <?= $row["comment"] ?><br>
                </td>
                <td>
                    <img src="images/<?= $row["image"] ?>" width=430 height=200>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<?php
include("footer.php");
?>
