<?php
include("header.php");

include("securimage/securimage.php");

?>
<div class="welcome">
    <p>Order the concert by Your choice and send e-mail:</p>
</div>
<div class="book">

    <form action="add-reservation.php" method="post">
        Your name:<br>
        <input type="text" name="name"<br>
        Your e-mail:<br>
        <input type="email" name="email"<br>
        Your phone:<br>
        <input type="tel" name="phone"><br>
        Concert date: <br>
        <input type="date" name="date"/><br>
        Concert time: <br>
        <input type="time" name="time"/><br>
        Concert duration (hours): <br>
        <input type="number" min="0" step="0.5" name="blength"/><br>
        Your message:<br>
        <textarea name="comment" value="your comment" maxlength="460" rows="10" cols="50"
                  size="50"></textarea><br>

        <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image"/><br>
        <input type="text" name="captcha_code" size="10" maxlength="6"/><br>

        <input type="submit" value="Send" onClick='emailSentMsg(this)'/>
        <input type="button" value="Reset" onClick='resetForm(this.parentNode)'/>
    </form>
</div><!-- book ends -->

<?php
include("footer.php");
?>

