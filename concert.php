<?php
include("header.php");
include("loginContent.php");

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    model_editConcert($_POST['edited'], $_POST['editedcomment']);
}
?>
    <div class="welcome">
        <p>Please fill all fields to add a concert!</p>
    </div>
    <div class="insert">
        <form method="POST" action="add-concerts.php">
            Concert's name: <br><input pattern=".{3,30}" type="text" name="name" required title="minimum 3 characters"/><br>
            Date: <br>
            <input type="date" name="date"/><br>
            Time: <br>
            <input type="time" name="time"/><br>
            Place: <br><input pattern=".{5,100}" type="text" name="address" title="minimum 5 characters"/><br>
            Comment: <br>
            <textarea name="comment" maxlength="460" rows="10" cols="50"></textarea><br>
            Image 430px*200px: <br><input type="file" name="image"/><br>
            <input name="rsubmit" type="submit" value="Save"/>
        </form>
    </div>
    <hr>
    <div class="look">
        <div class="inserted">
            <p><b>All concerts sorted by datetime.</b>
        </div>
        <table>
            <?php foreach (model_getConcerts() as $row): ?>
                <tr>
                    <td><a href="<?= $_SERVER['PHP_SELF'] ?>?edited=<?= $row["id"] ?>">Edit </a></td>
                    <td><a href="delete-concerts.php?id=<?= $row["id"] ?>">Delete </a></td>
                    <td class='series'>
                        <div class="title"><?= $row["name"] ?></div>
                        <?php
                        $date = DateTime::createFromFormat("Y-m-d H:i:s", $row["date"] . " " . $row["time"]);
                        echo $date->format("l, jS \of F Y \a\\t G.i");
                        ?><br>
                        <?= $row["address"] ?><br><br>
                        <?php
                        if (isset($_GET['edited'])):
                            if ($row['id'] == $_GET['edited']): ?>
                                <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                                    <input type="hidden" name='edit ed' value=<?= $row['id']; ?>>
                                    <textarea name="editedcomment" maxlength="460" rows="10"
                                              cols="50"><?= $row['comment'] ?></textarea>
                                    <button type="submit">Save</button>
                                </form><br>
                                <?php
                            else :
                                ?>
                                <?= $row["comment"] ?><br>
                                <?php
                            endif;
                        else: ?>
                            <?= $row["comment"] ?><br>
                            <?php
                        endif; ?>
                    </td>
                    <td>
                        <img src="images/<?= $row["image"] ?>" width=430 height=200>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

<?php
include("footer.php");
?>