<?php
session_start();

$host = 'localhost';
$user = 'test';
$pass = 't3st3r123';
$db = 'test';

$l = mysqli_connect($host, $user, $pass, $db);
mysqli_query($l, 'SET CHARACTER SET UTF8');

function anti_inject($str)
{
    global $l;
    return mysqli_real_escape_string($l, $str);
}

function model_getUser($username, $pw)
{
    $username = anti_inject($username);
    $password = anti_inject($pw);

    global $l;

    $query = 'SELECT id, password FROM iak_users WHERE username=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 's', $username);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $password);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    if ($password === $pw) {
        return true;
    }

    return false;
}

function model_getConcerts($numOfConcerts = -1)
{
    $numOfConcerts = anti_inject($numOfConcerts);
    global $l;
    $query = null;

    $query = "SELECT id, name, date, time, address, comment, image FROM  `iak_concert` ORDER BY  `date` ASC";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $name, $date, $time, $address, $comment, $image);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $dateTime = DateTime::createFromFormat("Y-m-d H:i:s", $date . " " . $time);
        $dateTime = $dateTime->getTimestamp();
        $dateTimeToday = time();

        if ($numOfConcerts > 0) {
            if ($dateTime > $dateTimeToday) {
                $numOfConcerts--;
                $rows[] = array(
                    'id' => $id,
                    'name' => $name,
                    'date' => $date,
                    'time' => $time,
                    'address' => $address,
                    'comment' => $comment,
                    'image' => $image,
                );
            }
        } else if ($numOfConcerts == -1) {
            if ($dateTime > $dateTimeToday) {
                $rows[] = array(
                    'id' => $id,
                    'name' => $name,
                    'date' => $date,
                    'time' => $time,
                    'address' => $address,
                    'comment' => $comment,
                    'image' => $image,
                );
            }
        }
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function model_addConcert($rname, $date, $time, $raddress, $comment, $image)
{
    $rname = anti_inject($rname);
    $date = anti_inject($date);
    $time = anti_inject($time);
    $raddress = anti_inject($raddress);
    $comment = anti_inject($comment);
    $image = anti_inject($image);

    global $l;

    $query = "INSERT INTO `iak_concert`(`id`, `name`, `date`, `time`, `address`, `comment`, `image`) VALUES ('','$rname', '$date', '$time','$raddress', '$comment','$image')";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;
}

function model_editConcert($id)
{
    $id = anti_inject($id);
    global $l;

    $query = 'UPDATE `iak_concert` SET name=? date=? time=? address=? comment=? image=? WHERE Id=?';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    $edited = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return $edited;
}

function model_deleteConcert($id)
{
    $id = anti_inject($id);
    global $l;

    $query = 'DELETE FROM iak_concert WHERE Id=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    $deleted = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return $deleted;
}

function model_addUser($username, $password1, $name)
{
    $username = anti_inject($username);
    $password1 = anti_inject($password1);
    $name = anti_inject($name);
    global $l;

    $query = "INSERT INTO iak_users(username, password, name) VALUES ('$username', '$password1', '$name')";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;

}

function model_getKanals($findId = -1)
{
    global $l;

    $query = "SELECT id,bron FROM iak_kanal";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $bron);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'bron' => $bron
        );
    }
    mysqli_stmt_close($stmt);

    return $rows;
}

/*function model_addReservation($bname, $date, $time, $number, $blength, $brname, $bron, $tel, $comment) {
    global $l;

    $query = "INSERT INTO `iak_reservation`(`id`, `rid`,`bname`, `date`, `time`, `number`, `blength`, `brname`, `bron`, `tel`, `comment`) VALUES ('','$bname','$date','$time','$blength','$number','$brname','$bron','$tel','$comment',NOW())";
 */
function model_addReservation($concertId, $startDate, $startTime, $duration, $personCount, $contactType, $contactName, $comment, $contact)
{
    $concertId = anti_inject($concertId);
    $startDate = anti_inject($startDate);
    $startTime = anti_inject($startTime);
    $duration = anti_inject($duration);
    $personCount = anti_inject($personCount);
    $contactType = anti_inject($contactType);
    $contactName = anti_inject($contactName);
    $comment = anti_inject($comment);
    $contact = anti_inject($contact);

    global $l;

    $query = "INSERT INTO `iak_reservation` (`id`, `concertId`, `startDate`, `startTime`, `duration`, `personCount`, `contactType`, `contactName`, `comment`, `contact`) VALUES ('','$concertId', '$startDate', '$startTime', '$duration', '$personCount', '$contactType', '$contactName', '$comment', '$contact')";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;
}

/*
function model_getReservations() {
    global $l;

    $query = "SELECT id, bname, date, time, number, blength, brname, tel, comment FROM iak_concert INNER JOIN iak_reservation ON iak_concert.id=iak_reservation.rid ORDER BY iak_reservation.date";
*/
function model_getReservations()
{
    global $l;

    $query = "SELECT iak_concert.name as concertName,
                    iak_reservation.concertId as concertId,
                    iak_reservation.startDate as startDate,
                    iak_reservation.startTime as startTime,
                    iak_reservation.duration as duration,
                    iak_reservation.personCount as personCount,
                    iak_reservation.contactName as contactName,
                    iak_reservation.contactType as contactType,
                    iak_reservation.contact as contact,
                    iak_reservation.comment as comment FROM iak_concert INNER JOIN iak_reservation ON iak_concert.id=iak_reservation.concertId ORDER BY iak_reservation.startDate";
    $stmt = mysqli_prepare($l, $query);

    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $concertName, $concertId, $startDate, $startTime, $duration, $personCount, $contactName, $contactType, $contact, $comment);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'concertName' => $concertName,
            'concertId' => $concertId,
            'startDate' => $startDate,
            'startTime' => $startTime,
            'duration' => $duration,
            'personCount' => $personCount,
            'contactName' => $contactName,
            'contactType' => $contactType,
            'contact' => $contact,
            'comment' => $comment
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}