<?php
include("header.php");
include("loginContent.php");
?>
    <div class="welcome">
        <p>Please fill all fields to add a reservation!</p>
    </div>

    <div class="reservationform">

        <form method="POST" action="add-reservation.php">
            Concert's name: <br>
            <select name="concertId">
                <?php foreach (model_getConcerts() as $row): ?>
                    <option value="<?= $row["id"] ?>"><?= $row["name"] ?></option>';
                <?php endforeach; ?>
            </select><br>
            Date: <br>
            <input type="date" name="startDate"/><br>
            Time: <br>
            <input type="time" name="startTime"/><br>
            Duration (hours): <br>
            <input type="number" min="0" step="0.5" name="duration"/><br>
            Number of persons: <br>
            <input type="number" min="1" max="500" step="1" name="personCount"/><br>
            Your Name: <br>
            <input pattern=".{2,50}" type="text" name="contactName"/><br>
            Reservation:
            <select name="contactType"><br>
                <?php foreach (model_getKanals() as $row): ?>
                    <option value="<?= $row["id"] ?>"><?= $row["bron"] ?></option>';
                <?php endforeach; ?>
            </select><br>
            Yor Contacts: <br>
            <input pattern=".{5,10}" type="text" name="contact"/><br>
            Comment: <br>
            <textarea name="comment" maxlength="460" rows="10" cols="50"></textarea><br>
            <input type="submit" name="rsubmit" value="Save"/>
        </form>
    </div>
    <hr>
    <div class="added-reservations">
        <div class="entered">
            <p><b>All reservations sorted by datetime.</b>
        </div>
        <?php
        if (isset($_SESSION["username"])):
            foreach (model_getReservations() as $row): ?>
                </a>Concert: <?= $row["concertName"] ?><br>
                Date and time: <?php
                $bdate = DateTime::createFromFormat("Y-m-d H:i:s", $row["startDate"]);
                echo $bdate->format("l, jS \of F Y \a\\t G.i");
                ?><br>
                Duration (hours): <?= $row["duration"] ?><br>
                Number of persons attending: <?= $row["personCount"] ?><br>
                Reservation by:
                <?php
                $typeId = $row["contactType"];
                $kanalArr = model_getKanals($typeId);
                echo $kanalArr[0]["bron"];
                ?> [<?= $row["contact"] ?>]<br>
                Contact: <?= $row["contactName"] ?><br>
                Comment: <?= $row["comment"] ?>
                <br><br>
                <a href="delete-reservations.php">Delete</a>
                <hr>
                <hr>
            <?php endforeach;
        endif; ?>
    </div>

<?php
include("footer.php");
?>